<?php

declare(strict_types = 1);

namespace Gupo\Preparation\Exceptions;

/**
 * 前置类 异常
 */
class PreparationException extends \Exception implements \Throwable
{
}
